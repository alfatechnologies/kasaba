$(document).ready(function(){
	function detTop(){
		var mainNav = $('.main-navbar');
		var top = $(window).scrollTop();
		if(top > 100) {
			mainNav.addClass('changed-navbar');
		} else {
			mainNav.removeClass('changed-navbar');
		}
	}
	$(window).scroll(function(){
		detTop();
	});
});